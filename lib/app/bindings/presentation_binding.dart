import 'package:citv_v1_app/app/controller/presentation_controller.dart';
import 'package:get/get.dart';

class PresentationBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PresentationController>(() => PresentationController());
  }
}
